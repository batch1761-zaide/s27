

// let numbers = [2,4,5,6,8];

// for(let i=0; i<numbers.length; i++){

// 	if(numbers[i] < 5) {
// 		if( numbers[i] == 2)
// 			console.log(`index [${i}] is equal to ${numbers[i]}!`);
// 		if( numbers[i] == 4)
// 			console.log(`index [${i}] is equal to 4!`);
// 		if( numbers[i] == 5)
// 			console.log(`index [${i}] is equal to 5!`);
// 	} else if (numbers[i] > 5){
// 		if( numbers[i] == 6)
// 			console.log(`index [${i}] is equal to 6!`);
// 		if( numbers[i] == 8)
// 			console.log(`index [${i}] is equal to 8!`);
// 	} else
// 			console.log(`index [${i}] is equal to 5!`);
// }

const http = require('http')
const products = require('./products')

console.log(products)

const server = http.createServer((req, res) => {

	if(req)

	
/*	//---------------- Longer version
	res.StatusCode = 200
	res.setHeader('Content-Type', 'text/html')
	res.write('<h1>Hello World!</h1>')
	res.end()*/

	
	if(req.url === '/products' && req.method === 'GET'){
		//---------------- Shorter version
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(products))
	} else {
		res.writeHead(404, {'Content-Type': 'application/json'})
		res.end(JSON.stringify({message: 'Route Not Found'}))
	}


})

const PORT = process.env.PORT || 5000

server.listen(PORT, () => console.log(`Server running on port ${PORT}`))


